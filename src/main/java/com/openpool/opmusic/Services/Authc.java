package com.openpool.opmusic.Services;

import com.openpool.opmusic.Vo.CodeEnum;
import com.openpool.opmusic.Vo.RestFulBody;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/21 10:25
 **/
@Service
public class Authc {

    public RestFulBody login(String username, String pwd) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, pwd,true);
        subject.login(usernamePasswordToken);
        return RestFulBody.mapInfoList(CodeEnum.LOGIN_SUCCESS, "登录", null);
    }
}
