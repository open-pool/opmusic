package com.openpool.opmusic.Services;

import com.openpool.opmusic.Untils.MailUtils.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/19 14:52
 **/
@Service
public class MailService {
    @Autowired
    MailUtils mailUtils;


    /**
     * 发送简单邮件
     * */
    public void sendMailToUser(String to,String title,String message){
        mailUtils.sendMain(to,title,message);
    }
    public void sendMailToUser(String from,String to,String title,String message){
        mailUtils.sendMain(from,to,title,message);
    }
}
