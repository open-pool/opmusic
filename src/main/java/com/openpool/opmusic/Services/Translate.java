package com.openpool.opmusic.Services;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 22:23
 **/
@Service
public class Translate {

    @Autowired
    RestTemplate restTemplate;

    private String URL ="https://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i={search}";

    public Map getData(String data){
        Map resp = new LinkedHashMap();
        Map parameter = new HashMap();
        parameter.put("search",data);
        String forObject = restTemplate.getForObject(URL, String.class, parameter);
        try {
            JSONObject jsonObject = new JSONObject(forObject);
            JSONArray translateResult = jsonObject.getJSONArray("translateResult");
            JSONArray jsonArray = translateResult.getJSONArray(0);
            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
            String src = jsonObject1.get("src").toString();
            String tgt = jsonObject1.get("tgt").toString();
            resp.put("original",src);
            resp.put("translate",tgt);
            return resp;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
