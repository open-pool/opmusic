package com.openpool.opmusic.Services;

import com.google.common.collect.Maps;
import com.openpool.opmusic.Vo.CodeEnum;
import com.openpool.opmusic.Vo.RestFulBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 9:31
 **/
@Service
public class GteMusicResource {

    @Autowired
    private RestTemplate restTemplate;

    private final String musicList = "https://music.163.com/api/search/get/web?csrf_token=hlpretag=&hlposttag=&s={search}&type=1&offset=0&total=true&limit=50";
    private final String musicMedia = "http://music.163.com/api/song/enhance/player/url?id={search}&ids=[{search}]&br=3200000";
    //    http://music.163.com/api/song/lyric?os=pc&id=85580&lv=-1&kv=-1&tv=-1
//    http://music.163.com/api/song/detail/?id=1879641033&ids=%5B1879641033%5D
    private final String musicInfo ="https://autumnfish.cn/song/detail?ids={search}";


    public List getMusic(String parameterUrl) {
        List resp = new ArrayList();
        Map parameter = new HashMap();
        parameter.put("search", parameterUrl);
        String forObject = restTemplate.getForObject(musicList, String.class, parameter);
        try {
            JSONObject jsonObject = new JSONObject(forObject);
            JSONObject rest = new JSONObject(jsonObject.get("result").toString());
            JSONArray songs = rest.getJSONArray("songs");
            for (int i = 0; i < songs.length(); i++) {
                JSONObject jsonObject1 = songs.getJSONObject(i);
                String id = jsonObject1.get("id").toString();
                String name = jsonObject1.get("name").toString();

                parameter.put("search", id);
                String forObject1 = restTemplate.getForObject(musicMedia, String.class, parameter);
                JSONObject jsonObject2 = new JSONObject(forObject1);
                JSONArray data = jsonObject2.getJSONArray("data");

                String forObject2 = restTemplate.getForObject(musicInfo, String.class, parameter);
                JSONObject jsonObject3 = new JSONObject(forObject2);
                JSONArray songs1 = jsonObject3.getJSONArray("songs");
                JSONObject jsonObject4 = songs1.getJSONObject(0);
                String img = jsonObject4.getJSONObject("al").get("picUrl").toString();
                String author = jsonObject4.getJSONArray("ar").getJSONObject(0).get("name").toString();

                Map restinfo = new LinkedHashMap();
                restinfo.put("name", name);
                restinfo.put("img", img);
                restinfo.put("author", author);
                restinfo.put("mediaUrl", data.getJSONObject(0).get("url").toString());
                resp.add(restinfo);
            }
            return resp;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public RestFulBody getMedia(String mediaId) throws JSONException {
        Map parameter = new HashMap();
        parameter.put("search", mediaId);
        String forObject = restTemplate.getForObject(musicMedia, String.class, parameter);
        JSONObject jsonObject = new JSONObject(forObject);
        String media = jsonObject.getJSONArray("data").getJSONObject(0).get("url").toString();
        String jsoninfo= restTemplate.getForObject(musicInfo, String.class, parameter);
        JSONObject songs = new JSONObject(jsoninfo).getJSONArray("songs").getJSONObject(0);
        String name = songs.get("name").toString();
        String img = songs.getJSONObject("al").get("picUrl").toString();
        String author = songs.getJSONArray("ar").getJSONObject(0).get("name").toString();
        HashMap<String, String> resoInfo = Maps.newLinkedHashMap();
        resoInfo.put("name",name);
        resoInfo.put("author",author);
        resoInfo.put("media",media);
        resoInfo.put("img",img);
        return RestFulBody.mapInfoList(CodeEnum.GET_SUCCESS, "网易云音乐", resoInfo);
    }
}
