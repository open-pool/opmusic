package com.openpool.opmusic.Advice;

import com.openpool.opmusic.Vo.CodeEnum;
import com.openpool.opmusic.Vo.RestFulBody;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/17 8:52
 **/
@Component
@RestControllerAdvice
public class ControllerAdviceInfo {

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public RestFulBody exce(Exception e){
        return RestFulBody.error(CodeEnum.EXCEPTION_FAILING,e.getMessage());
    }


}
