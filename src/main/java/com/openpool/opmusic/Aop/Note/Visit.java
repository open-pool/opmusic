package com.openpool.opmusic.Aop.Note;

import java.lang.annotation.*;

/**
 * @Author: 王居三木超
 * @Description: 参与验证
 * @DateTime: 2021/9/19 17:51
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Visit {
    boolean request()default false;
}
