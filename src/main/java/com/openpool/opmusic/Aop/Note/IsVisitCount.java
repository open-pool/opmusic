package com.openpool.opmusic.Aop.Note;

import java.lang.annotation.*;
/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/22 10:30
 **/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsVisitCount {
    boolean required() default true;
}
