package com.openpool.opmusic.Aop;

import com.openpool.opmusic.Aop.Note.IsVisitCount;
import com.openpool.opmusic.Task.InitTask;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/19 17:50
 **/
@Aspect
@Component
public class VisitCount {

    @Pointcut("@annotation(com.openpool.opmusic.Aop.Note.Visit)")
    private void visit() {
    }

    @Pointcut("@annotation(com.openpool.opmusic.Aop.Note.IsVisitCount)")
    private void visitCount() {
    }

    @Around("visit()")
    public Object visitAround(ProceedingJoinPoint joinPoint) throws Throwable {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes attributes = (ServletRequestAttributes) requestAttributes;
        return joinPoint.proceed();
    }

    @After("visitCount()")
    public void visitCountAfte(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        IsVisitCount fun = signature.getMethod().getAnnotation(IsVisitCount.class);
        if (fun.required()) {
            InitTask.count++;
            InitTask.todayCount++;
        }
    }
}
