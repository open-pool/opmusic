package com.openpool.opmusic.Vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/25 9:41
 **/
@AllArgsConstructor
@Data
@NoArgsConstructor
public class IndexData {
    private String desc;
    private String descApi;
    private String meathod;
    private String testUrl;
    private String gotoUrl;
}
