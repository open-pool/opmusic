package com.openpool.opmusic.Vo;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 19:35
 **/
public enum  CodeEnum {
    GET_SUCCESS("获取成功",0),
    GET_FAILING("获取失败",1),
    SEND_SUCCESS("发送成功",0),
    SEND_FAILING("发送失败",1),
    LOGIN_SUCCESS("登录成功",0),
    LOGIN_FAILING("登录失败",1),
    EXCEPTION_FAILING("服务器异常",-1);
    private String desc;
    private Integer code;

    CodeEnum(String desc,Integer code){
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getCode() {
        return code;
    }
}
