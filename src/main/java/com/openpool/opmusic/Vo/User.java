package com.openpool.opmusic.Vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/18 18:19
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String username;
    private String pwd;
}
