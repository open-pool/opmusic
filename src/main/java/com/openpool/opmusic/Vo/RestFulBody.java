package com.openpool.opmusic.Vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 11:50
 **/
@Data
@ApiModel("信息返回体")
public class RestFulBody<T> {
    private String organization = "OpenPool";
    private String author = "王居三木超,弓长二月鸟";
    private String gitee = "https://gitee.com/open-pool";
    private Integer code;
    private String desc;
    private String type;
    private T data;

    public static RestFulBody musicInfoList(CodeEnum codeEnum, String type,List data) {
        RestFulBody<List> listRestFulBody = new RestFulBody<>();
        listRestFulBody.data = data;
        listRestFulBody.code = codeEnum.getCode();
        listRestFulBody.desc = codeEnum.getDesc();
        listRestFulBody.type = type;
        return listRestFulBody;
    }

    public static RestFulBody mapInfoList(CodeEnum codeEnum, String type,Map data) {
        RestFulBody<Map> listRestFulBody = new RestFulBody<>();
        listRestFulBody.data = data;
        listRestFulBody.code = codeEnum.getCode();
        listRestFulBody.desc = codeEnum.getDesc();
        listRestFulBody.type = type;
        return listRestFulBody;
    }

    public static RestFulBody error(CodeEnum codeEnum,String data) {
        RestFulBody<String> listRestFulBody = new RestFulBody<>();
        listRestFulBody.code = codeEnum.getCode();
        listRestFulBody.desc = codeEnum.getDesc();
        listRestFulBody.data = data;
        listRestFulBody.type = "异常";
        return listRestFulBody;
    }
}
