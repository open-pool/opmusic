package com.openpool.opmusic.Controller;

import com.openpool.opmusic.Aop.Note.IsVisitCount;
import com.openpool.opmusic.Task.InitTask;
import com.openpool.opmusic.Vo.CodeEnum;
import com.openpool.opmusic.Vo.IndexData;
import com.openpool.opmusic.Vo.RestFulBody;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 16:15
 **/
@Controller
@ApiIgnore
public class Page {

    ArrayList<IndexData> data = Lists.newArrayList();
    {
        String Get = "[GET]";
        String Post = "[POST]";
        String OPOOL = "[OpenPool接口]";
        data.add(new IndexData("搜索歌曲实例","[网易云接口]",Get,
                "http://localhost:6999/getMusic?content=许嵩",
                "/getMusic?content=许嵩"));
        data.add(new IndexData("搜索单曲信息实例","[网易云接口]",Get,
                "http://localhost:6999/getMusicInfo?id=1879641033",
                "/getMusicInfo?id=1879641033"));
        data.add(new IndexData("英汉互译实例","[网易有道接口]",Get,
                "http://localhost:6999/tranlate?data=我是谁呢",
                "/tranlate?data=我是谁呢"));
    }


    @GetMapping("/")
    @IsVisitCount
    public String visitIndex(Model model){
        model.addAttribute("count",InitTask.count);
        model.addAttribute("oldCount",InitTask.oldCount);
        model.addAttribute("todayCount",InitTask.todayCount);
        model.addAttribute("data",data);
        return "index.html";
    }

    @RequestMapping("/count")
    @ResponseBody
    @IsVisitCount
    public RestFulBody visitCount(){
        Map info = new HashMap<>();
        info.put("count", InitTask.count);
        info.put("oldCount",InitTask.oldCount);
        return RestFulBody.mapInfoList(CodeEnum.GET_SUCCESS,"访问",info);
    }
}
