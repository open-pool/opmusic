package com.openpool.opmusic.Controller;

import com.openpool.opmusic.Services.Authc;
import com.openpool.opmusic.Vo.RestFulBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/21 10:46
 **/
@RestController
@ApiIgnore
public class Auth {

    @Resource
    Authc authc;

    @PostMapping("/login")
    public RestFulBody logn(String username, String pwd) {
        return authc.login(username,pwd);
    }
}
