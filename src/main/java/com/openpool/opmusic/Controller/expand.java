package com.openpool.opmusic.Controller;

import com.openpool.opmusic.Aop.Note.IsVisitCount;
import com.openpool.opmusic.Services.MailService;
import com.openpool.opmusic.Services.Translate;
import com.openpool.opmusic.Task.InitTask;
import com.openpool.opmusic.Vo.CodeEnum;
import com.openpool.opmusic.Vo.RestFulBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.websocket.server.PathParam;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 23:20
 **/
@RestController
@Api(tags = "拓展功能")
public class expand {

    @Resource
    Translate translate;
    @Resource
    MailService mailService;

    @ApiOperation("英汉互译")
    @GetMapping("tranlate")
    @IsVisitCount
    public RestFulBody translateData(@ApiParam("待翻译文本") @PathParam("data") String data) {
        return RestFulBody.mapInfoList(CodeEnum.GET_SUCCESS, "翻译"+ InitTask.count, translate.getData(data));
    }

    @ApiOperation("邮箱发送")
    @GetMapping("mailSend")
    @IsVisitCount
    public RestFulBody mailSend(@ApiParam("收件人") @PathParam("to") String to,
                                @ApiParam("邮件标题") @PathParam("title") String title,
                                @ApiParam("内容") @PathParam("message") String message) {
        Map resp = new HashMap();
        try {
            mailService.sendMailToUser(to, title, message);
            resp.put("type", "ok");
            return RestFulBody.mapInfoList(CodeEnum.SEND_SUCCESS, "mail", resp);
        } catch (Exception e) {
            resp.put("type", "error");
            return RestFulBody.mapInfoList(CodeEnum.SEND_FAILING, "mail", resp);
        }
    }

    @ApiOperation("邮箱发送,自定义发送人")
    @GetMapping("mailSendDef")
    @IsVisitCount
    public RestFulBody mailSendDef(@ApiParam("收件人") @PathParam("from") String from,
                                   @ApiParam("收件人") @PathParam("to") String to,
                                   @ApiParam("邮件标题") @PathParam("title") String title,
                                   @ApiParam("内容") @PathParam("message") String message) {
        Map resp = new HashMap();
        try {
            mailService.sendMailToUser(from, to, title, message);
            resp.put("type", "ok");
            return RestFulBody.mapInfoList(CodeEnum.SEND_SUCCESS, "mail", resp);
        } catch (Exception e) {
            resp.put("type", "error");
            return RestFulBody.mapInfoList(CodeEnum.SEND_FAILING, "mail", resp);
        }
    }
}
