package com.openpool.opmusic.Controller;

import com.openpool.opmusic.Aop.Note.IsVisitCount;
import com.openpool.opmusic.Services.GteMusicResource;
import com.openpool.opmusic.Vo.CodeEnum;
import com.openpool.opmusic.Vo.RestFulBody;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.json.JSONException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.websocket.server.PathParam;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/16 9:38
 **/
@RestController
@CrossOrigin
@Api(tags = "获取音乐公共接口")
public class GetMusic {

    @Resource
    GteMusicResource gteMusicResource;

    @ApiOperation(value = "搜索音乐")
    @GetMapping("getMusic")
    @IsVisitCount
    public RestFulBody getMsic(@ApiParam("搜索字段") @PathParam("content")String content){
        return RestFulBody.musicInfoList(CodeEnum.GET_SUCCESS,"网易云音乐",gteMusicResource.getMusic(content));
    }
    @ApiOperation(value = "搜索单曲详细信息")
    @GetMapping("getMusicInfo")
    @IsVisitCount
    public RestFulBody getMsicInfo(@ApiParam("搜索歌曲id") @PathParam("id")String id) throws JSONException {
        return gteMusicResource.getMedia(id);
    }
}
