package com.openpool.opmusic.Task;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/19 15:51
 **/
@Component
public class Tasks {

    private final String COUNT_TIME = "0 */20 * * * ?";
    private final String COUNT_TIME_UPDATE = "0 59 23 * * ?";
    private Logger logger = LoggerFactory.getLogger(Tasks.class);
    private ThreadPoolExecutor executor = new ThreadPoolExecutor(
            1, 1, 1L, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(1),
            new ThreadPoolExecutor.CallerRunsPolicy()
    );

    @Scheduled(cron = COUNT_TIME)
    public void visitCount() throws IOException {
        logger.info("更新访问文件");
        FileWriter fileWriter = new FileWriter(InitTask.countFile);
        Map info = new HashMap();
        info.put("count", InitTask.count);
        info.put("oldCount", InitTask.oldCount);
        info.put("todayCount", InitTask.todayCount);
        fileWriter.write(JSON.toJSON(info).toString());
        fileWriter.flush();
        fileWriter.close();
    }

    @Scheduled(cron = COUNT_TIME_UPDATE)
    public void updateVisitCount() throws IOException {
        logger.info("更新每日访问次数");
        InitTask.oldCount = InitTask.todayCount;
        InitTask.todayCount = 0;
        visitCount();
    }
}
