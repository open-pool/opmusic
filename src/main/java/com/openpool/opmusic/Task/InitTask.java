package com.openpool.opmusic.Task;

import com.alibaba.fastjson.JSON;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import sun.misc.IOUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/19 16:09
 **/
@Component
public class InitTask implements ApplicationRunner {

    public static Integer count;
    public static Integer todayCount;
    public static Integer oldCount;
    public static File countFile = new File("count");
    public static FileSystemResource fileSystemResource;
    {
        JSONObject jsonObject = null;
        if (!InitTask.countFile.exists()) {
            try {
                InitTask.countFile.createNewFile();
                fileSystemResource = new FileSystemResource(InitTask.countFile);
                Map info = new HashMap();
                info.put("count",0);
                info.put("oldCount",0);
                info.put("todayCount",0);

                jsonObject =new JSONObject(JSON.toJSON(info).toString());
                FileWriter fileWriter = new FileWriter(countFile);
                fileWriter.write(jsonObject.toString());
                fileWriter.flush();
                fileWriter.close();
                jsonObject.toString();
                count = 0;
                oldCount = 0;
                todayCount = 0;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            try {
                fileSystemResource = new FileSystemResource(InitTask.countFile);
                String JSONSource = new String(IOUtils.readFully(fileSystemResource.getInputStream(), -1, true),"utf-8");
                jsonObject = new JSONObject(JSONSource);
                count = (Integer) jsonObject.get("count");
                oldCount = (Integer) jsonObject.get("oldCount");
                todayCount = (Integer) jsonObject.get("todayCount");
            }  catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }
}
