package com.openpool.opmusic.Config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/19 11:49
 **/
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/").setViewName("index.html");//主页
        registry.addViewController("/musciitems.html").setViewName("musciitems.html");//音乐实例页
        registry.addViewController("/login.html").setViewName("login.html");//登录实例页
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new InterConfig())
                .addPathPatterns("/**");
    }
}
