package com.openpool.opmusic.Config;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

/**
 * @Author: 王居三木超
 * @Description: 拦截器Shiro
 * @DateTime: 2021/9/21 9:33
 **/
@Configuration
public class ShiroConfig {

    private final String SIGN = "2AvVhdsgUs0FSA3SDFAdag==";

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier("SecManager") DefaultWebSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        /**
         * 启动拦截
         * */
        filterChainDefinitionMap.put("/img/**", "anon");
        filterChainDefinitionMap.put("/error/**", "anon");
        filterChainDefinitionMap.put("/favicon.ico", "anon");
        filterChainDefinitionMap.put("/login", "anon");
        filterChainDefinitionMap.put("/count", "anon");

        filterChainDefinitionMap.put("/swagger-ui.html/**", "anon");
        filterChainDefinitionMap.put("/webjars/**", "anon");
        filterChainDefinitionMap.put("/swagger-resources/**", "anon");
        filterChainDefinitionMap.put("/v2/**", "anon");

        filterChainDefinitionMap.put("/", "anon");
        filterChainDefinitionMap.put("/getMusic", "anon");
        filterChainDefinitionMap.put("/getMusicInfo", "anon");
        filterChainDefinitionMap.put("/tranlate", "anon");

        filterChainDefinitionMap.put("/mailSend", "user");

        filterChainDefinitionMap.put("/**", "authc");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        shiroFilterFactoryBean.setLoginUrl("/login.html");
        return shiroFilterFactoryBean;
    }


    @Bean(value = "SecManager")
    public DefaultWebSecurityManager defaultWebSecurityManager(@Qualifier("shiroRam") ShiroRealm shiroRam) {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(shiroRam);
        defaultWebSecurityManager.setRememberMeManager(rememberMeManager());
        return defaultWebSecurityManager;
    }

    public CookieRememberMeManager rememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(simpleCookie());
        cookieRememberMeManager.setCipherKey(Base64.getDecoder().decode(SIGN));
        return cookieRememberMeManager;
    }

    public SimpleCookie simpleCookie() {
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        simpleCookie.setMaxAge(25920000);
        return simpleCookie;
    }

    @Bean
    public ShiroRealm shiroRam() {
        return new ShiroRealm();
    }
}
