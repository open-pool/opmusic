package com.openpool.opmusic.Config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/18 12:13
 **/
@Service
public class EsConfig {


    @Value("${spring.elasticsearch.host}")
    private String REMOTEHOST;

    @Bean
    public RestHighLevelClient client(){
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(REMOTEHOST,9200,"http")
                )
        );
        return client;
    }
}
