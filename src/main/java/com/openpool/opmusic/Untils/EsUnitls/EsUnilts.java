package com.openpool.opmusic.Untils.EsUnitls;

import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: 王居三木超
 * @Description: ES工具类
 * @DateTime: 2021/9/18 16:49
 **/
@Component
public class EsUnilts {

    @Autowired
    RestHighLevelClient client;

    private RequestOptions options = RequestOptions.DEFAULT;

    public boolean createIndex(String indexName) throws IOException {
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName);
        return client.indices().create(createIndexRequest, options).isAcknowledged();
    }

    public boolean deleteIndex(String indexName) throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indexName);
        return client.indices().delete(deleteIndexRequest, options).isAcknowledged();
    }

    public boolean existIndex(String indexName) throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest(indexName);
        return client.indices().exists(getIndexRequest, options);
    }

    public int insertDocment(String indexName, String id, Object data) throws IOException {
        IndexRequest indexRequest = new IndexRequest(indexName);
        indexRequest.id(id);
        indexRequest.source(JSON.toJSONString(data), XContentType.JSON);
        IndexResponse index = client.index(indexRequest, options);
        return index.status().getStatus();
    }

    public boolean insertDocments(String indexName, String id, List data) throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("2s");
        for (int i = 0; i < data.size(); i++) {
            bulkRequest.add(
                    new IndexRequest(indexName)
                            .id(""+i)
                            .source(JSON.toJSONString(data.get(i)),XContentType.JSON)
            );
        }
        return client.bulk(bulkRequest, options).hasFailures();
    }

    public int deleteDocment(String indexName, String id) throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest(indexName, id);
        deleteRequest.timeout("1s");
        return client.delete(deleteRequest, options).status().getStatus();
    }

    public int updateDocment(String indexName, String id, Object data) throws IOException {
        UpdateRequest updateRequest = new UpdateRequest(indexName, id);
        updateRequest.doc(JSON.toJSONString(data), XContentType.JSON);
        UpdateResponse update = client.update(updateRequest, options);
        return update.status().getStatus();
    }

    public Map<String, Object> getDocment(String indexName, String id) throws IOException {
        GetRequest getRequest = new GetRequest(indexName, id);
        GetResponse documentFields = client.get(getRequest, options);
        return documentFields.getSource();
    }

    public boolean existDocment(String indexName, String id) throws IOException {
        GetRequest getRequest = new GetRequest(indexName, id);
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        getRequest.storedFields("_none_");
        return client.exists(getRequest, options);
    }

    public SearchHit[] searchDocment(String indexName, String field, String value) throws IOException {
        SearchRequest searchRequest = new SearchRequest(indexName);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery(field, value);
        searchSourceBuilder.query(fuzzyQueryBuilder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, options);
        return search.getHits().getHits();
    }

}
