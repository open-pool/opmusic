package com.openpool.opmusic.Untils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author: 王居三木超
 * @Description: TODO
 * @DateTime: 2021/9/19 17:10
 **/
@Component
public class TokenUtils {
    static private String SIGN = "FGSAGDSGJIDKSJGMKLDS*^&**JNFDNSJIHFSDA";

    public String getToken(Long time) {
        Date now = new Date();
        return JWT.create()
                .withIssuer("OpenPool")
                .withIssuedAt(now)
                .withExpiresAt(new Date(now.getTime()+6000))
                .withClaim("oldTime", System.currentTimeMillis())
                .sign(Algorithm.HMAC256(SIGN));
    }

    public DecodedJWT valicationToken(String token){
        return JWT.decode(token);
    }
}
