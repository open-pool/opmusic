# opmusic

#### 介绍

[![star](https://gitee.com/open-pool/opmusic/badge/star.svg?theme=dark)](https://gitee.com/open-pool/opmusic/stargazers)[![fork](https://gitee.com/open-pool/opmusic/badge/fork.svg?theme=dark)](https://gitee.com/open-pool/opmusic/members)

封装网易云api，实现自定义接口

[![OpenPool/opmusic](https://gitee.com/open-pool/opmusic/widgets/widget_card.svg?colors=eae9d7,2e2f29,272822,484a45,eae9d7,747571)](https://gitee.com/open-pool/opmusic)

#### 软件架构
**前端**，vue

**后端**，springboot，Es，JavaMail


#### 安装教程

1.  opmusic\target\下找到opmusic-1.0-SNAPSHOT.jar
2. java -jar opmusic-1.0-SNAPSHOT.jar
3. 默认端口6999

#### 版本说明

**v1.1**

1. 实现/getMusic?content={搜索内容}接口，搜索音乐


#### DEV-更新说明

1. 提升了fastjson依赖版本
2. 更新了/tranlate?data={搜索内容}接口，英汉互译
3. 添加了ES搜索引擎
4. 添加了Shiro验证
5. 添加音乐控件[http://music.openpool.cn/musciitems.html](http://music.openpool.cn/musciitems.html).
